import React, {useContext} from 'react';
import {View, TouchableOpacity, Image} from "react-native";
import {Portal, Modal} from "react-native-paper";
import {GalleryContext} from "../context/gallery";

const ModalPhoto = () => {

  const SGallery = useContext(GalleryContext)

  return (
    <Portal>
      <Modal visible={SGallery.visible} onDismiss={() => SGallery.setVisible(!SGallery.visible)}>
        <TouchableOpacity
          onPress={() => SGallery.setVisible(!SGallery.visible)}
          style={{paddingHorizontal: 10, paddingVertical: 10}}>
          <Image
            resizeMode="contain"
            source={{uri: SGallery.photoInModal?.fileUri}}
            style={{width: '100%', height: '100%'}}
          />
        </TouchableOpacity>
      </Modal>
    </Portal>
  );
};

export default ModalPhoto;
