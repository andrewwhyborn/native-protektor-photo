import React, {useContext, useState} from 'react';
import main from "../assets/styles/main";
import {View, TouchableOpacity, Image, Text, StyleSheet} from "react-native";
import {Portal, Modal, Button} from "react-native-paper";
import {BarCodeContext} from "../context/barcode";
import {BarCodeReadEvent, RNCamera} from "react-native-camera";
import {useCamera} from "react-native-camera-hooks";

const ModalBarCode = () => {

  const SBarCode = useContext(BarCodeContext)
  const [{cameraRef}, {takePicture}] = useCamera()
  return (
    <Portal>
      <Modal visible={SBarCode.visible} onDismiss={() => SBarCode.toggleModalBarCode()}>
        <TouchableOpacity
          onPress={() => SBarCode.toggleModalBarCode()}
          style={{paddingHorizontal: 10, paddingVertical: 10, alignItems: 'center'}}>
          <RNCamera
            ref={cameraRef}
            type={RNCamera.Constants.Type.back}
            // flashMode={RNCamera.Constants.FlashMode.on}
            style={styles.cam}
            barCodeTypes={[RNCamera.Constants.BarCodeType.ean13]}
            onBarCodeRead={( barcodes: BarCodeReadEvent ) => {
              // console.log(barcodes);
              // console.log(barcodes.bounds);
              SBarCode.setBarCode(barcodes.data)
            }}>
          </RNCamera>

          {SBarCode.barCode.length > 0
          &&
          <View style={{width: '100%'}}>
            <Text style={main.BarCodeText}>{SBarCode.barCode}</Text>
            <Button
              onPress={()=>SBarCode.saveBarCode(SBarCode.barCode)}
              style={{marginVertical: 10, marginTop: 'auto'}}
              icon="content-save-edit-outline"
              mode="contained">
              Сохранить код
            </Button>
          </View>
          }

        </TouchableOpacity>
      </Modal>
    </Portal>
  );
};

const styles = StyleSheet.create({

  btn:{
    width: '100%',
    height: 50,
    backgroundColor: '#ccc'
  },
  cam: {
    borderRadius: 5,
    overflow: 'hidden',
    width: 300,
    height: 300,
    // flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    // borderWidth: 1,
    // borderColor: 'red'
  }

})
export default ModalBarCode;
