import React, {useContext, useEffect, useState} from 'react';
import main from "../assets/styles/main";
import {Image, PermissionsAndroid, TouchableOpacity, View} from "react-native";
import {Button} from "react-native-paper";
import Icon from "react-native-vector-icons/FontAwesome";
import {CameraLaunch, RemovePhotoInDir} from "../util/helper";
import {IPhoto} from "../types";
import {GalleryContext} from "../context/gallery";

const Gallery = () => {

  const SGallery = useContext(GalleryContext)

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Доступ к камере",
          message: "Приложению необходим доступ к вашей камере",
          buttonNeutral: "Спроси Меня Позже",
          buttonNegative: "Отмена",
          buttonPositive: "Дать доступ"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Camera permission given");
        return true
      } else {
        console.log("Camera permission denied");
        return false
      }
    } catch (err) {
      console.warn(err);
      return false
    }
  };

  const openCamera = async () => {
    let permission = await requestCameraPermission()
    if (!permission) return
    await CameraLaunch().then((res) => {
      if (Object.keys(res).length === 0) return
      SGallery.setPhotos([...SGallery.photos, res])
    })
  }

  const removePhoto = (fileName: string, fileUri: string) => {
    let newData = SGallery?.photos.filter((item )=> item.fileName !== fileName)
    RemovePhotoInDir(fileUri)
    SGallery.setPhotos(newData)
  }

  const openPhotoInModal = (photo: IPhoto) => {
    SGallery.setVisible(!SGallery.visible)
    SGallery.setPhotoInModal(photo)
  }

  useEffect(() => {
    SGallery.readFileInCache()
  }, [])

  const list = SGallery?.photos?.map((item) => {

    return (
      <View
        style={[main.AddPhotoCol]}
        key={item.fileName}>

        <View style={[main.AddPhoto]}>
          <TouchableOpacity
            onPress={() => removePhoto(item.fileName, item.fileUri)}
            style={[main.PhotoRemove]}>
            <Icon name="trash" size={15} color="#fff"/>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => openPhotoInModal(item)}
          >
            <Image
              source={{uri: item?.fileUri}}
              resizeMode="cover"
              style={{width: 100, height: 100, borderRadius: 10}}
            />
          </TouchableOpacity>

        </View>

      </View>
    )
  })


  return (
    <View style={[main.AreaAddPhoto]}>
      {list}
      <View style={[main.AddPhotoCol]}>
        <View style={[main.AreaAddPhoto]}>
          <TouchableOpacity
            onPress={() => openCamera()}
            style={[main.AddPhoto]}>
            <Button
              // icon="camera"
              // labelStyle={[{fontSize: 60}]}
              style={[main.AddPhotoButton]}>
              <Icon name="camera" size={60} color="#6200ee"/>
            </Button>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Gallery;
