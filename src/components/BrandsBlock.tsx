import React, {FC, useContext, useEffect, useState} from 'react';
import {View, Alert, ActivityIndicator, SafeAreaView} from "react-native";
import {IBrand, ISelectBrand} from "../types";

// @ts-ignore
import SearchableDropdown from 'react-native-searchable-dropdown';
import {UserContext} from "../context/user";
import main from "../assets/styles/main";

const BrandsBlock: FC = () => {

  const SUser = useContext(UserContext)

  useEffect(() => {
    SUser.getBrand()
  }, [])

  const handleChangeSelectBrand = async (data: IBrand) => {

    await SUser.setSelectBrand(data)
    await SUser.setSelectModel({})
    await SUser.setModelsOptionList([])
    await SUser.getModel(data.id)
  }

  const handleChangeNameBrand = async (name: string) => {

    if (name.length === 0) {
      SUser.setSelectBrand({})
    } else {
      SUser.setSelectBrand({...SUser.selectBrand, name})
    }
  }
  if (SUser.loadingBrand) return <View><ActivityIndicator/></View>

  return (
    <View style={{marginVertical: 5}}>
      <SearchableDropdown
        // fixAndroidTouchableBug={true}
        containerStyle={[main.containerStyle]}
        itemStyle={main.itemStyle}
        itemTextStyle={{color: '#222'}}
        itemsContainerStyle={main.itemsContainerStyle}
        // defaultIndex={itemindex}
        textInputProps={
          {
            value: SUser.selectBrand.name,
            placeholder: "Выберите бренд",
            underlineColorAndroid: "transparent",
            style: main.textInputProps,
            onTextChange: (text: string) => handleChangeNameBrand(text)
          }
        }
        listProps={
          {
            nestedScrollEnabled: true,
          }
        }
        items={SUser.brandsOptionList}
        onItemSelect={(item: object, index: number) => {
          handleChangeSelectBrand((item as ISelectBrand))
        }}
      />
    </View>
  );
};

export default BrandsBlock;
