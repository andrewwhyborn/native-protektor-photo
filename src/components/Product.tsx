import React, {FC, useContext} from 'react';
import {View, ScrollView, Text, ActivityIndicator} from 'react-native'
// @ts-ignore
import SearchableDropdown from 'react-native-searchable-dropdown';
import main from "../assets/styles/main";
import {UserContext} from "../context/user";

const Product: FC = () => {

  const SUser = useContext(UserContext)

  const handleChangeSelectProduct = (data: any) => {
    console.log('data', data)
    SUser.setSelectProduct(data)
  }

  const handleChangeNameProduct = (name = '', id = null) => {
    SUser.setSelectProduct({...SUser.selectProduct, name})
  }

  if (SUser.loadingProduct) return (
    <View style={{marginVertical: 10}}>
      <ActivityIndicator/>
    </View>
  )

  if(SUser.productOptionList.length === 0) return <View><Text>Список продуктов пуст</Text></View>

  return (
    <View style={{marginTop: 5, marginBottom: 10}}>
      <SearchableDropdown
        // fixAndroidTouchableBug={false}
        containerStyle={[main.containerStyle]}
        itemStyle={main.itemStyle}
        itemTextStyle={{color: '#222'}}
        itemsContainerStyle={main.itemsContainerStyle}
        // defaultIndex={2}
        // resetValue={true}
        textInputProps={
          {
            value: SUser.selectProduct.name,
            placeholder: "Выберите модель",
            underlineColorAndroid: "transparent",
            style: main.textInputProps,
            onTextChange: (text = '') => handleChangeNameProduct(text)
          }
        }
        listProps={
          {
            nestedScrollEnabled: true,
          }
        }
        items={SUser.productOptionList}
        onItemSelect={(item: {}) => {
          handleChangeSelectProduct(item)
        }}
      />
    </View>

  );
};

export default Product;
