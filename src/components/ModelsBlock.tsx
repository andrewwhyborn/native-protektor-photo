import React, {FC, useContext, useEffect, useState} from 'react';
import {View, Alert, ActivityIndicator, Text} from "react-native";
import api from '../api/index'
import {formatResponseOptions} from "../util/helper";
import {IBrand, IBrands} from "../types";
// @ts-ignore
import SearchableDropdown from 'react-native-searchable-dropdown';
import {UserContext} from "../context/user";
import main from '../assets/styles/main'

const ModelsBlock: FC = () => {

  const SUser = useContext(UserContext)

  const handleChangeSelectModel = (data: any) => {
    SUser.setSelectModel(data)
    SUser.getProduct(SUser.selectBrand?.id, data.id)
  }

  const handleChangeNameModel = (name = '', id = null) => {
    SUser.setSelectModel({...SUser.selectModel, name})
  }

  useEffect(()=>{
    if(SUser?.selectModel?.name?.length === 0){
      SUser.setSelectModel({})
    }
  }, [SUser.selectModel.name])


  if (SUser.loadingModel) return <View style={{marginVertical: 10}}><ActivityIndicator/></View>

  if(SUser.modelsOptionList.length === 0) return <View><Text>Список моделей пуст</Text></View>

  return (
    <View style={{marginVertical: 5}}>
      <SearchableDropdown
        // fixAndroidTouchableBug={false}
        containerStyle={[main.containerStyle]}
        itemStyle={main.itemStyle}
        itemTextStyle={{color: '#222'}}
        itemsContainerStyle={main.itemsContainerStyle}
        defaultIndex={0}
        // resetValue={true}
        textInputProps={
          {
            value: SUser.selectModel.name,
            placeholder: "Выберите модель",
            underlineColorAndroid: "transparent",
            style: main.textInputProps,
            onTextChange: (text = '') => handleChangeNameModel(text)
          }
        }
        listProps={
          {
            nestedScrollEnabled: true,
          }
        }
        items={SUser.modelsOptionList}
        onItemSelect={(item: {}) => {
          handleChangeSelectModel(item)
        }}
      />
    </View>
  );
};

export default ModelsBlock;
