import React, {FC, useEffect, useState} from 'react';
import {Snackbar} from "react-native-paper";
import {View, Text} from "react-native";

interface IPopUp {
  text?: string
  flag: boolean
}

const PopUp: FC<IPopUp> = ({text,flag}) => {
  console.log('flag',flag)
  // console.log(text)
  const [visibleSnack, setVisibleSnack] = useState(flag);
  // console.log('visibleSnack', visibleSnack)
  // useState(()=>{
  //   setTimeout(()=>{
  //     setVisibleSnack(false)
  //   }, 1500)
  // },[flag])


  return (
    <Snackbar
      visible={visibleSnack}
      onDismiss={() => setVisibleSnack(false)}
      duration={1500}
      action={{
        label: '',
        onPress: () => {
          setVisibleSnack(false)
        }
      }}>
      {text}
    </Snackbar>
  );
};

export default PopUp;
