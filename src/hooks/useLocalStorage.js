import { useState, useEffect } from "react";
import AsyncStorage from '@react-native-async-storage/async-storage';

async function getStorageValue(key, defaultValue) {
  // getting stored value
  console.log(key, defaultValue)
  const saved =  await AsyncStorage.getItem(key);
  console.log('saved', saved)
  const initial = JSON.parse(saved);
  return initial || defaultValue;
}

export const useLocalStorage = (key, defaultValue) => {
  const [value, setValue] = useState(() => {
    return getStorageValue(key, defaultValue);
  });

  useEffect(() => {
    // storing input name
    const init = ()=>{
       AsyncStorage.setItem(key, JSON.stringify(value));
    }
    init()
  }, [key, value]);

  return [value, setValue];
};
