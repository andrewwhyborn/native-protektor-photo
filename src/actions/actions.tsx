import React, {createRef} from "react";
import {Alert, BackHandler} from "react-native";
import {useContext, useEffect} from "react";

export const ExitApp = () =>{
  const handleBackButton = () => {
    Alert.alert(
      'Выход',
      'Закрыть приложение?', [{
        text: 'Нет',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'Да',
        onPress: () => {
          BackHandler.exitApp()
        }
      },],
      {
        cancelable: false
      }
    )
    return true;
  }

  useEffect(() => {
    const subscription = BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    return () => {
      subscription.remove()
    }

  }, [])
}
export const LogOut = async () => {
  console.log('logOut')

}
