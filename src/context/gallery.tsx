import React, {FC, createContext, ReactNode, useState, Dispatch} from 'react';
import * as RNFS from "react-native-fs";
import {dirCache, RemoveCachePhotos, WatchDir} from "../util/helper";
import * as type from '../types'

interface IGallery {
  photos: Array<type.IPhoto>;
  photoInModal: type.IPhoto | null;
  visible: boolean | false
  setVisible(visible: boolean): void;
  readFileInCache: () => void;
  setPhotos(newData: any): void;
  setPhotoInModal(photo: any): void;
  resetGallery(): void
}

export const GalleryContext = createContext({} as IGallery);

export const GalleryContextProvider = ({children}: type.PropsChildrenContext) => {

  const [photos, setPhotos] = useState([])
  const [photoInModal, setPhotoInModal] = useState(null);

  const [visible, setVisible] = useState(false)

  const readFileInCache = async () => {
    let files: any = []
    await RNFS.readdir(dirCache).then((data) => {
      files = WatchDir(data)
      console.log(data)
      setPhotos(files)
    }).catch((err) => {
      console.log(err.message);
    })
  }

  const resetGallery = () => {
    setPhotos([])
    setPhotoInModal(null)
    setVisible(false)
    RemoveCachePhotos(photos)
  }

  return (
    <GalleryContext.Provider
      value={{
        readFileInCache,
        photos, setPhotos,
        photoInModal, setPhotoInModal,
        visible, setVisible,
        resetGallery
      }}>
      {children}
    </GalleryContext.Provider>
  );

}
