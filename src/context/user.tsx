import React, {createContext, ReactNode, useContext, useEffect, useState} from 'react';
import * as type from '../types';
import api from '../api';
import {formatResponseProduct, formatResponseOptions} from '../util/helper';
import {LogOut} from '../actions/actions';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as RootNavigation from '../actions/RootNavigation.js';
import {ISelectProduct} from "../types";


export const UserContext = createContext({} as type.IUserContext);

export const UserContextProvider = ({children}: type.PropsChildrenContext) => {

  const [user, setUser] = useState(null);

  const [brandsOptionList, setBrandsOptionList] = useState([]);
  const [selectBrand, setSelectBrand] = useState({} as type.ISelectBrand);
  const [loadingBrand, setLoadingBrand] = useState(true);

  const [modelsOptionList, setModelsOptionList] = useState([]);
  const [selectModel, setSelectModel] = useState({} as type.ISelectModel);
  const [loadingModel, setLoadingModel] = useState(true);

  const [productOptionList, setProductOptionList] = useState([]);
  const [selectProduct, setSelectProduct] = useState({} as ISelectProduct)
  const [loadingProduct, setLoadingProduct] = useState(true)

  const getBrand = async () => {
    setLoadingBrand(true)
    await api.getBrand().then((response) => {
      // @ts-ignore
      setBrandsOptionList(formatResponseOptions(response))
      setLoadingBrand(false)
    }).catch(e => {

    })
  }

  const getModel = async (id: number) => {
    if (!id) {
      return;
    }
    setLoadingModel(true)
    await api.getModel(id).then((response: any) => {
      // @ts-ignore
      setModelsOptionList(formatResponseOptions(response));
      setLoadingModel(false)
    }).catch(e => {
      setLoadingModel(false)
    });
  };

  const getProduct = async (brand_id: number, model_id: number) => {

    if (!brand_id && !model_id) {
      return;
    }
    setLoadingProduct(true)

    await api.getProduct(brand_id, model_id).then((response: any) => {
      // @ts-ignore
      setProductOptionList(formatResponseProduct(response))
      setLoadingProduct(false)
    }).catch(e => {
      console.log(e)
      setLoadingProduct(false)
    });
  };

  const LogOut = async () => {

    try {
      await AsyncStorage.removeItem('token');
      axios.defaults.headers.common['Authorization'] = '';
      await RootNavigation.navigate('Login');
      setUser(null);
    } catch (error) {
      console.log('error', error);
    }
  };

  const resetBrandModel = () => {
    setSelectBrand({} as type.ISelectBrand);
    setSelectModel({} as type.ISelectModel);
    setSelectProduct({} as type.ISelectProduct)
    setProductOptionList([])
    setModelsOptionList([]);

  };

  return (
    <UserContext.Provider
      value={{
        user, setUser,

        brandsOptionList, setBrandsOptionList,
        selectBrand, setSelectBrand,
        loadingBrand, setLoadingBrand,

        modelsOptionList, setModelsOptionList,
        selectModel, setSelectModel,
        loadingModel, setLoadingModel,

        productOptionList, setProductOptionList,
        selectProduct, setSelectProduct,
        loadingProduct, setLoadingProduct,

        getBrand,
        getModel,
        getProduct,
        LogOut,
        resetBrandModel,
      }}>
      {children}
    </UserContext.Provider>
  );
};
