import React, {FC, createContext, ReactNode, useState, Dispatch} from 'react';
import * as type from "../types";

interface IBarCode {
  barCode: string;
  setBarCode(barCode: string): void;
  visible: boolean;
  setVisible(visible: boolean): void;
  savedBarCode: string;
  setSavedBarCode(savedBarCode: string): void;

  toggleModalBarCode(): void;

  saveBarCode(code: string): void;

  resetContext(): void;

}

export const BarCodeContext = createContext({} as IBarCode);

export const BarCodeContextProvider = ({children} : type.PropsChildrenContext) => {

  const [barCode, setBarCode] = useState("")
  const [visible, setVisible] = useState(false)
  const [savedBarCode, setSavedBarCode] = useState('')

  const toggleModalBarCode = () => {
    setVisible(!visible)
    setBarCode('')
  }

  const saveBarCode = (code:string) => {
    setSavedBarCode(code)
    toggleModalBarCode()
  }
  const resetContext = () => {
    setBarCode('')
    setSavedBarCode('')
  }
  return (
    <BarCodeContext.Provider
      value={{
        barCode, setBarCode,
        visible,setVisible,
        toggleModalBarCode,
        setSavedBarCode,
        savedBarCode,
        saveBarCode,
        resetContext
      }}>
      {children}
    </BarCodeContext.Provider>
  );

}
