import React, {FC, useContext, useEffect, useState} from 'react';
import {ScrollView, View, Text, KeyboardAvoidingView, Alert} from "react-native";
import api from '../api/index';
import {Button, ActivityIndicator} from "react-native-paper";
import {IPropsMain} from "../types";
import {UserContext} from "../context/user";
import {GalleryContext} from "../context/gallery";
import {BarCodeContext} from "../context/barcode";
import PopUp from "../components/PopUp";
import ModalPhoto from "../components/ModalPhoto";
import ModalBarCode from "../components/ModalBarCode";
import BrandsBlock from "../components/BrandsBlock";
import ModelsBlock from "../components/ModelsBlock";
import Product from "../components/Product"
import Gallery from "../components/Gallery";
import main from "../assets/styles/main";
import {formatResponseOptions} from "../util/helper";

const Main: FC<IPropsMain> = ({}) => {

  const SUser = useContext(UserContext)
  const SGallery = useContext(GalleryContext)
  const SBarCode = useContext(BarCodeContext)
  const [sendAccess, setSendAccess] = useState(false)
  const [sendLoading, setSendLoading] = useState(false)

  const logOut = async (): Promise<void> => {
    await SUser.LogOut()
  }

  const sendData = async () => {
    setSendLoading(true)
    setSendAccess(false)

    let formdata = new FormData();
    formdata.append(
      'model_id', SUser?.selectModel?.id
    )
    formdata.append(
      'user_id', SUser.user?.id,
    )
    formdata.append(
      'articul', SUser.selectProduct?.articul,
    )
    formdata.append(
      'barcode', SBarCode?.savedBarCode,
    )
    SGallery.photos.forEach((file, index) => {
      let data = {
        uri: file.fileUri,
        name: file.fileName,
        filename: file.fileName,
        type: file.type
      }
      console.log('file', data)
      formdata.append('file[]', data);
    });

    await api.sendData(formdata).then(res => {
      setSendAccess(true)
      SUser.resetBrandModel()
      SGallery.resetGallery()
      SBarCode.resetContext()
      SUser.getBrand()
    }).catch(e => {
      console.log('error', e)
      Alert.alert(
        "Ошибка",
        "Что-то пошло не так",
        [
          {
            text: "Отмена",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "OK", onPress: () => console.log("OK Pressed") }
        ]
      );
    }).finally(()=>{
      setSendLoading(false)
    })

    // setTimeout(()=>{
    //   setSendAccess(true)
    //   setSendLoading(false)
    //   SUser.resetBrandModel()
    //   SGallery.resetGallery()
    //   SBarCode.resetContext()
    //   api.getBrand().then((response) => {
    //     // stateUser.setBrands(formatResponse(response));
    //     SUser.setBrandsOptionList(formatResponseOptions(response))
    //   }).catch(e => {
    //
    //   })
    // },2000)

  }

  return (

      <View style={[main.Area]} >
        <Button
          onPress={() => logOut()}
          style={{marginVertical: 10, marginTop: 'auto'}}
          icon="arrow-left"
          mode="contained">
          Выход
        </Button>

        {sendAccess
        &&
        <PopUp
          flag={true}
          text="Данные отправлены"
        />
        }

        <ScrollView
          keyboardShouldPersistTaps='always'
          style={[{paddingTop: 10}]}
          contentContainerStyle={[main.ScrollContainer, {paddingBottom: 10}]}>
          <View style={[main.Container]}>

            <BrandsBlock/>

            {SUser?.selectBrand?.id
            &&

            <ModelsBlock/>

            }

            {SUser?.selectBrand?.id && SUser.selectModel?.id
            &&

            <Product/>

            }
            <View style={{marginVertical: 10}}>
              <Gallery/>
            </View>

            {SBarCode?.savedBarCode.length > 0
            &&
            <View>
              <Text style={main.BarCodeText}>
                {SBarCode.savedBarCode}
              </Text>

            </View>
            }


          </View>

        </ScrollView>

        <View style={[main.AreaButton]}>
          <Button
            onPress={() => SBarCode.toggleModalBarCode()}
            style={{marginVertical: 10, marginTop: 'auto', position: 'relative', zIndex: 1}}
            icon="barcode-scan"
            mode="contained">
            Сканировать штрих-код
          </Button>
        </View>

        {SGallery?.photos?.length > 0 && SUser?.selectBrand?.id && SUser?.selectModel?.id
        &&
        <View style={[main.AreaButton]}>
          {sendLoading
            ?
            <ActivityIndicator/>
            :
            <Button
              onPress={() => sendData()}
              style={{marginVertical: 10, marginTop: 'auto'}}
              icon="file-send"
              mode="contained">
              Отправить фотки
            </Button>
          }
        </View>
        }

        <ModalPhoto/>
        <ModalBarCode/>

      </View>


  );
};

export default Main;
