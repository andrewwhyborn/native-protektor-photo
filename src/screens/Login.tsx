import React, {useState, useEffect, useContext, useCallback} from 'react';
import {SafeAreaView, ScrollView, Text, View, ActivityIndicator} from "react-native";
import {useFocusEffect} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {Button, TextInput,} from "react-native-paper";
import Icon from 'react-native-vector-icons/FontAwesome';
import {UserContext} from "../context/user";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {NavigationList} from "../types";
import axios from 'axios';
import main from "../assets/styles/main";
import api from '../api';
import PopUp from "../components/PopUp";

interface ILogin {
  navigation: StackNavigationProp<NavigationList>;
}

//protektor@mail.ru
//Qq111111
interface IErrors {
  email?: string
  password?: string
}

const Login = ({navigation}: ILogin) => {
  const stateUser = useContext(UserContext)
  const [loading, setLoading] = useState(true)
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordOpen, setPasswordOpen] = useState(false)
  const [errors, setErrors] = useState<IErrors>({});
  const [isLoading, setIsLoading] = useState(false);
  const [loadEnter, setLoadEnter] = useState(false)

  useFocusEffect(
    useCallback(() => {
      const init = async () => {
        const token = await AsyncStorage.getItem('token');
        if (!!token) {
          // console.log('есть токен', token)
          axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
          try {
            const res = await api.getMe();
            console.log(res)
            stateUser.setUser(res.user);
            navigation.navigate('Main')
            // navigation.navigate('Test')
            // setLoading(false)
          } catch (e: any) {
            console.log(e)
          }
        } else {
          setLoading(false)
        }
      };
      init();
    }, [])
  );

  const login = async () => {
    try {
      setErrors({});
      setIsLoading(true);
      setLoadEnter(true)
      const data = {email, password};
      // console.log(data)
      const res = await api.login(data);
      // console.log(res)
      stateUser.setUser(res.user);
      await AsyncStorage.setItem('token', res.user.token);
      axios.defaults.headers.common['Authorization'] = `Bearer ${res.user.token}`;
      navigation.navigate('Main')
      setIsLoading(false);
      setEmail('');
      setPassword('');

    } catch (e: any) {
      setIsLoading(false);

      if (e?.response?.data) {
        console.log('e.response.data', e.response.data);

        if (e?.response?.data?.message) {
          setErrors({password: e.response.data.message});
        }

        if (e?.response?.data?.error) {
          setErrors(e.response.data.error);
        }
      } else {
        console.log('error', e);
      }

    } finally {
      setLoadEnter(false)
    }
  }

  if (loading) return <SafeAreaView style={[main.Container, main.CenterV, main.Area]}>
    <ActivityIndicator size="small" color="#0000ff"/>
  </SafeAreaView>

  return (

    <SafeAreaView style={[main.Container, main.CenterV, main.Area]}>
      {Object.keys(errors).reverse().map((item) => {
        return (
          <PopUp
            flag={true}
            text={(errors[item as keyof IErrors])}/>
        )
      })}
      <ScrollView
        keyboardShouldPersistTaps='always'
        style={[{paddingTop: 10}]}
        contentContainerStyle={[main.ScrollContainer,main.CenterV,  {paddingBottom: 10}]}>
        <TextInput
          mode="outlined"
          label="Email"
          value={email}
          onChangeText={(text: string) => setEmail(text)}
        />


        <TextInput
          mode="outlined"
          label="Пароль"
          value={password}
          secureTextEntry={!passwordOpen && true}
          onChangeText={(text: string) => setPassword(text)}
          right={
            <TextInput.Icon name={() => <Button
              // mode="contained"
              onPress={() => setPasswordOpen(!passwordOpen)}>
              {
                passwordOpen
                  ?
                  <Icon name="eye" size={18} color="#6200ee"/>
                  :
                  <Icon name="eye-slash" size={18} color="#6200ee"/>
              }

            </Button>}/>
          }
        />
        <View>
          {loadEnter
            ?
            <ActivityIndicator/>
            :
            <Button
              style={{marginVertical: 10}}
              icon="exit-run"
              mode="contained"
              onPress={() => login()}>
              Вход
            </Button>
          }
        </View>
      </ScrollView>
    </SafeAreaView>

  );
};

export default Login;
