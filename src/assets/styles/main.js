'use strict';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  test: {
    borderWidth: 1,
    borderColor: 'red',
  },
  Area: {
    position: 'relative',
    flex: 1,
    backgroundColor: '#fff',
  },
  ScrollContainer: {
    flexGrow: 1,
    backgroundColor: '#fff',
  },
  Container: {
    flex: 1,
    width: '100%',
    paddingRight: 20,
    paddingLeft: 20,
    zIndex: 2,
    position: 'relative',
  },
  CenterV: {
    justifyContent: 'center',
  },
  AreaAddPhoto: {
    // marginTop: 5,
    // borderWidth: 1,
    // borderColor: 'red',
    zIndex: 1,
    position: 'relative',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: '-2%',

  },
  PhotoRemove: {
    position: 'absolute',
    top: -5,
    right: -5,
    zIndex: 5,
    width: 25,
    height: 25,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ccc',
  },
  AddPhotoCol: {
    flexBasis: '33.333%',
    paddingHorizontal: 10,

  },
  AddPhoto: {
    marginBottom: 20,
    // overflow: 'hidden',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    // width: '100%',
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 'auto',
    width: '100%',
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    // marginHorizontal: 10
  },
  AddPhotoButton: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ImageStyle: {
    width: '100%',
    height: '100%',
  },
  AreaButton: {
    // position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%',
    paddingHorizontal: 20,
  },
  itemStyle: {
    padding: 10,
    marginTop: 0,
    backgroundColor: '#fff',
    borderBottomColor: '#ccc',
    borderRightColor: '#ccc',
    borderLeftColor: '#ccc',
    borderBottomWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderRadius: 0,
  },
  containerStyle: {
    width: '100%',
    position: 'absolute',
    marginBottom: 30,
    height: 60,
  },
  itemsContainerStyle:{
    position: 'absolute',
    zIndex: 10,
    maxHeight: 150,
    top: 50,
    width: '100%',
    backgroundColor: '#fff',
  },
  textInputProps:{
    backgroundColor: '#fff',
    padding: 12,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 0,
  },
  BarCodeText:{
    backgroundColor: '#fff',
    fontSize: 24,
    color: '#000',
    textAlign: 'center',
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginVertical: 5,
    borderRadius: 5
  },
});
