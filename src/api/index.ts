import axios from 'axios';

let url = 'https://home.protektor57.ru/market_api'

export default {
  // пользователь
  login: async (data: object) => axios.post('https://market.protektor57.ru/users/login', data).then(res => res.data),
  getMe: async () => axios.get('https://market.protektor57.ru/users/me').then(res => res.data),

  //Список брендов
  /*Список брендов по легковику (по необходимости можно передать
  GET параметр type с одним из значений:
  gruz, moto, tyre, и получить данные не только по легковику).*/
  getBrand: async () => axios.get(`${url}/brand_list/`).then(res => res.data),

  //Список моделей
  /*
    Список моделей по легковику (по необходимости можно передать GET параметр
    type с одним из значений: gruz, moto, tyre, и получить данные не только по легковику).
     Второй не обязательный параметр brand_id, чтобы получить модели по определенному бренду.
     */
  getModel: async (id: any) => axios.get(`${url}/model_list/?brand_id=${id}`).then(res => res.data),

  /*
  * brand_id - обязательный
  * model_id -обязательный
  * При возможности, можно ввести Артикул (articul)
  */
  getProduct: async (brand_id: number, model_id: number) => axios.get(`${url}/size_list/?brand_id=${brand_id}&model_id=${model_id}`).then(res => res.data),

  /*
  * передается 2 параметра. model_id (обязательно) - индификатор модели.
  * user_id
  * file (обязательно) - файл (если передается ссылкой, то вместо file использовать url)
  * articul
  * barcode
  * */
  sendData: async (data: object) => axios.post(`https://home.protektor57.ru/market_api/model_list/add/`, data).then(res => res.data),
}
