// @ts-ignore
import React, {useEffect} from "react";
import main from "../assets/styles/main";
import {NavigationContainer} from '@react-navigation/native';
// @ts-ignore
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../screens/Login'
import Main from '../screens/Main'
import Test from '../screens/Test'
import {ExitApp} from "../actions/actions";

import  {navigationRef} from '../actions/RootNavigation'

const Stack = createStackNavigator();

const MainStack: React.FC = () => {
  ExitApp()
  return (
    <NavigationContainer
      ref={navigationRef}>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName="Login">
        <Stack.Screen name="Login" component={Login}/>
        <Stack.Screen name="Main" component={Main}/>
        <Stack.Screen name="Test" component={Test}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MainStack;


