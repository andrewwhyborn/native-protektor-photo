import {ImageURISource} from 'react-native'
import {NavigatorScreenParams} from '@react-navigation/native'
import {ReactNode} from "react";

export interface PropsChildrenContext {
  children: ReactNode | ReactNode[];
}

export type NavigationList = {
  Main: undefined,
  Test: undefined
}

export interface IBrand {
  id: number | 0
  name: string | ''
}

export interface IBrands {
  name: any;
  brand?: IBrand
}

export interface IPropsMain {
  navigation: any
}

export interface IPhoto {
  type: string | ''
  fileName: string | ''
  fileUri: string | ''
}

export interface IUser {
  email: string | ''
  id: number | 0
  name: string | ''
}

export interface ISelectBrand {
  id: number | 0
  name: string | ''
}

export interface ISelectModel {
  id: number | 0
  name: string | ''
}

export interface ISelectProduct {
  id: number | 0
  name: string | ''
  articul: string | ''
}


export interface IUserContext {
  user: IUser | null;

  setUser(data: any): void

  brandsOptionList: Array<ISelectBrand>

  setBrandsOptionList(data: any): void

  selectBrand: ISelectBrand

  setSelectBrand(data: any): void

  loadingBrand: boolean | true;

  setLoadingBrand(data: boolean): void

  modelsOptionList: Array<object> | []

  setModelsOptionList(data: any): void

  selectModel: ISelectModel

  setSelectModel(data: any): void

  loadingModel: boolean | true

  setLoadingModel(data: boolean): void

  productOptionList: Array<object> | []

  setProductOptionList(data: any): void

  selectProduct: ISelectProduct

  setSelectProduct(data: any): void

  loadingProduct: boolean | true

  setLoadingProduct(data: boolean): void

  getModel(data: number): void

  resetBrandModel(): void

  getProduct(brand_id: number, model_id: number): void

  getBrand(): void

  LogOut(): void
}
