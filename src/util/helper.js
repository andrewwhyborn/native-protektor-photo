import * as RNFS from 'react-native-fs';
import {CameraOptions} from 'react-native-image-picker/src/types';
import {launchCamera} from 'react-native-image-picker';
import {IPhoto} from "../types";

export const dirCache = 'file:///data/user/0/com.nativeprotektorphoto/cache/';

export const WatchDir = (data) => {
  let result = [];
  for (let i = 0; i < data.length; i++) {
    let current = data[i];
    if (current.substr(-4) === '.jpg') {
      let obj = {};
      // obj.fileName = data[i].substr(0, data[i].length - 4);
      obj.fileName = data[i];
      obj.fileUri = `${dirCache}${data[i]}`;
      obj.type = "image/jpeg";
      // console.log('weewewe', obj)
      result.push(obj);
    }
  }
  return result;
};
export const RemovePhotoInDir = async (uri) => {
  // console.log('RemovePhotoInDir uri', uri);
  await RNFS.unlink(`${uri}`)
    .then(() => {
      console.log('FILE DELETED');
    })
    .catch((err) => {
      console.log(err.message);
    });

};

export const RemoveCachePhotos = async (data) => {
  await RNFS.readdir(dirCache).then((data) => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].substr(-4) === '.jpg') {
        RNFS.unlink(`${dirCache}${data[i]}`)
          .then(() => {
            console.log('FILE DELETED');
          })
          .catch((err) => {
            console.log(err.message);
          });
      }
    }
  });
};

export const CameraLaunch = async () => {

  let photo = {}

  let options = {
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  await launchCamera(options, (res) => {
    // console.log(res)
    if (res.didCancel || !res) return
    // console.log('res', res)
    photo = {
      // file: res?.assets[0],
      type:  res?.assets[0].type,
      fileName: res?.assets[0].fileName,
      fileUri: res?.assets[0].uri,
    }
  });

  return photo
}
export const formatResponseOptions =(response)=>{


  let arrayData = []
  let result = []
  Object.keys(response).forEach(item => {
    arrayData.push(response[item])
  })

  arrayData.forEach((item, index)=>{
    result.push( {name: item.name, id: item.id })
  })

  return result
}

export const formatResponseProduct =(response)=>{

  let result = []
  Object.keys(response).forEach((item, index )=> {
    let data = {
      id: index + 1,
      name: response[item],
      articul: item
    }
    result.push(data)
  })
  // console.log(result)
  return result
}
