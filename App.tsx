import React from 'react';
import { LogBox } from 'react-native';
import {UserContextProvider} from './src/context/user';
import {GalleryContextProvider} from "./src/context/gallery";
import {BarCodeContextProvider} from "./src/context/barcode";

import MainStack from "./src/stack/MainStack";

LogBox.ignoreLogs(['new NativeEventEmitter']); // Ignore log notification by message
LogBox.ignoreAllLogs(); //Ignore all log notifications

// @ts-ignore
// axios.defaults.baseURL = 'https://market.protektor57.ru/';

const App = () => {
  return (
    <UserContextProvider>
      <GalleryContextProvider>
        <BarCodeContextProvider>
          <MainStack/>
        </BarCodeContextProvider>
      </GalleryContextProvider>
   </UserContextProvider>
  );
};

export default App;
